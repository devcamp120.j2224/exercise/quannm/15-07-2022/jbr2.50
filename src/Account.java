public class Account {
    int id;
    Customer customer;
    double balance = 0.0;

    public Account(int id, Customer customer) {
        this.id = id;
        this.customer = customer;
    }
    public Account(int id, Customer customer, double balance) {
        this(id, customer);
        this.balance = balance;
    }
    public int getId() {
        return id;
    }
    public Customer getCustomer() {
        return customer;
    }
    public double getBalance() {
        return balance;
    }
    public void setBalance(double balance) {
        this.balance = balance;
    }
    public String getCustomerName() {
        return customer.getName();
    }
    public double deposit(double amount) {
        this.balance = balance + amount;
        return this.balance;
    }
    public double withdraw(double amount) {
        if (balance >= amount) {
            this.balance = balance - amount;
        }
        else {
            System.out.println("Amount withdrawn exceeds the current balance!");
        }
        return this.balance;
    }
    @Override
    public String toString() {
        return customer.getName() + "(" + getId() + ") balance=$" + getBalance();
    }
}
