public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        Customer customer1 = new Customer(1, "Quan", 10);
        Customer customer2 = new Customer(2, "Boi", 15);
        System.out.println(customer1 + "," + customer2);

        Account account1 = new Account(3, customer1);
        Account account2 = new Account(4, customer2, 200.25);
        System.out.println(account1 + "," + account2);

        account1.deposit(85);
        account2.withdraw(120);
        System.out.println("Balance new=" + account1 + "," + account2);
    }
}